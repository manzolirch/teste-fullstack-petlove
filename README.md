# Petlove Teste

Project stack - Client Side:

- Create React APP

Project stack - Server Side:

- Express
- Concurrently

## What do you need?

- Node.js
- NPM or Yarn
- Nodemon Globally

## Working on project

Here are the commands that are used in the project:

| Command    | Description                                                                                                                  |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------- |
| yarn dev   | Run the project in development with watch (run yarn dev in root project to run client side and server side at the same time) |
| yarn build | Build project                                                                                                                |
