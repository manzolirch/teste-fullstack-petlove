const express = require('express');
const fetch = require('node-fetch');

const app = express();
const port = process.env.PORT || 5000;

app.get('/api/buscar-cep/:cep', async (req, res) => {
  const cep = req.params.cep;
  const url = `https://viacep.com.br/ws/${cep}/json/`;

  const getCEP = async () => {
    const response = await fetch(url);

    let body = await response.json();

    if (response.status !== 200) throw Error('error');

    return body;
  };

  getCEP()
    .then(data => {
      if (data.erro) res.status(406);

      const { cep, logradouro, localidade, uf } = data;
      res.send({ cep, logradouro, localidade, uf });
    })
    .catch(err => console.error(err));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
