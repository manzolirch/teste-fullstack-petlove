import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { mount } from './enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('form submit', () => {
  const app = mount(<App />);

  app
    .find('form')
    .find('button')
    .simulate('click');
});
