import React from 'react';

// CSS
import './style.css';

function Button() {
  return (
    <button type="submit" className="search__button">
      Buscar CEP
    </button>
  );
}

export default Button;
