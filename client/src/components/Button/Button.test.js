import React from 'react';
import { shallow } from '../../enzyme';
import Button from './index';

describe('Button tests', () => {
  it('should render', () => {
    const button = shallow(<Button />);
    expect(button.exists()).toBeTruthy();
  });
});
