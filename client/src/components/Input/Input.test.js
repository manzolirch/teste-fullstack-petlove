import React from 'react';
import { shallow } from '../../enzyme';
import Input from './index';

describe('Input tests', () => {
  const input = shallow(<Input />);

  it('should render', () => {
    expect(input.exists()).toBeTruthy();
  });
});
