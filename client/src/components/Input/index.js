import React from 'react';
import PropTypes from 'prop-types';

// CSS
import './style.css';

function Input(props) {
  return <input type="tel" maxLength="9" placeholder="00000-000" {...props} />;
}

Input.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default Input;
