import React, { useState } from 'react';
import PropTypes from 'prop-types';

// Components
import Input from './components/Input';
import Button from './components/Button';

// CSS
import './reset.css';

function App() {
  const [cep, setCep] = useState('');
  const [error, setError] = useState('');
  const [data, setData] = useState({});

  const handleSubmit = e => {
    e.preventDefault();

    if (!cep) {
      setError('Por favor, digite um CEP');
    } else if (cep.length <= 8) {
      setError('Digite um CEP válido!');
    } else {
      setError('');

      const newCep = cep.replace(/[^\d]+/g, '');

      fetchCEP(newCep);
    }
  };

  const handleInput = value => {
    value = value.replace(/^(\d{5})(\d)/, '$1-$2');

    if (value.length >= 8) {
      setError('');
    }

    setCep(value);
  };

  async function fetchCEP(cep) {
    const response = await fetch(`/api/buscar-cep/${cep}`);

    if (response.status === 406) setError('CEP inválido');

    if (response.status === 500) {
      setError('A consulta de CEP falhou');
    } else {
      const data = await response.json();

      setData(data);
      setCep('');
    }
  }

  return (
    <section className="main">
      <div className="search-content">
        <form className="search-content-form" onSubmit={handleSubmit}>
          <label className="search-content-form-label">
            <Input
              value={cep}
              onChange={e => handleInput(e.target.value.replace(/\D/, ''))}
              className={
                error
                  ? 'search-content-form-label__input error'
                  : 'search-content-form-label__input'
              }
            />
            {error && (
              <span className="search-content-form__error">{error}</span>
            )}
          </label>
          <Button />
        </form>
      </div>

      <div
        className={
          data && !Object.keys(data).length
            ? 'result-content'
            : 'result-content active'
        }
      >
        <ul className="result-content-items">
          <li className="result-content-item">
            <strong>CEP:</strong> {data && data.cep}
          </li>
          <li className="result-content-item">
            <strong>Estado:</strong> {data && data.uf}
          </li>
          <li className="result-content-item">
            <strong>Cidade:</strong> {data && data.localidade}
          </li>
          <li className="result-content-item">
            <strong>Logradouro:</strong> {data && data.logradouro}
          </li>
        </ul>
      </div>
    </section>
  );
}

App.propTypes = {
  data: PropTypes.object
};

export default App;
